Source: freehep-graphicsio-pdf
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders:
 Giovanni Mascellani <gio@debian.org>,
 Philipp Huebner <debalance@debian.org>
Build-Depends:
 debhelper (>= 10),
 default-jdk,
 maven-debian-helper
Build-Depends-Indep:
 junit,
 libfreehep-graphicsio-java,
 libfreehep-graphicsio-tests-java
Standards-Version: 4.1.1
Vcs-Git: https://anonscm.debian.org/git/pkg-java/freehep/freehep-graphicsio-pdf.git
Vcs-Browser: https://anonscm.debian.org/git/pkg-java/freehep/freehep-graphicsio-pdf.git
Homepage: http://java.freehep.org/

Package: libfreehep-graphicsio-pdf-java
Architecture: all
Depends: ${maven:Depends}, ${misc:Depends}
Recommends: ${maven:OptionalDepends}
Description: FreeHEP Portable Document Format Driver
 The GraphicsIO library offers a base interface for image exporters in many
 vector or bitmap image formats. It features direct support for GIF, PNG, PPM
 and RAW formats, as well as the ability to manage TrueType fonts. Support
 for other file types can be added with plugins.
 .
 This package contains a GraphicsIO plugin to export to Portable Document
 Format.
 .
 FreeHEP is a collection of Java libraries used in High Energy Physics.
